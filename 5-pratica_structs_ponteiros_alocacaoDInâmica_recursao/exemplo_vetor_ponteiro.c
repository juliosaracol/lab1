#include <stdio.h>
#include <stdlib.h>

int main(){

int vet1[10]; //alocação estática
int *vet2; // alocação dinâmica

vet2 = (int *) malloc(10*sizeof(int));

//vet2 = &(vet1);
int i;
for(i=0;i<10;i++){
	vet1[i] = i;
//	vet2[i] = 2*i;
}

printf("v1:");
for(i=0;i<10;i++){
	printf("%d ",vet1[i]);
}
printf("\n");
printf("v2:");
for(i=0;i<10;i++){
	printf("%d ",vet2[i]);
}

return 0;
}
