#include <stdio.h>
#include <stdlib.h>

void impressao(int tamanho, int vetor[]);
void impressaoP(int tamanho, int *vetor);

int main(){

int vet[10];
int i;
for(i=0;i<10;i++
){
	vet[i]=i;	
}
impressao(10,vet);
impressaoP(10,
vet);

return 0;
}

// passagem por valor
// passagem por referência

void impressao(int tamanho, int vetor[]){
	int i;
	for(i=0;i<10;i++){
		printf("%d - ",vetor[i]);	
	}
	printf("\n");
}

void impressaoP(int tamanho, int *vetor){
	int i;
	
	for(i=0;i<10;i++){
		printf("%d - ",vetor[i]);	
	}
	printf("\n");
}

