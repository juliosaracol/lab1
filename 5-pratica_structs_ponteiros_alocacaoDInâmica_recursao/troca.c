#include <stdio.h>
#include <stdlib.h>

void troca(int h,int y); //passagem de parametro por copia ou valor
void trocaP(int *a,int *b); //passagem de parametro por referencia


int main(){

int a=1;
int b=2;
printf("valor a=%d e valor b=%d\n",a,b); //1,2
troca(a,b);
printf("depois de troca valor a=%d e valor b=%d\n",a,b);//2,1 
trocaP(&a,&b);
printf("depois trocaP valor a=%d e valor b=%d\n",a,b);
return 0;
}

void troca(int h,int y){
int aux = h;
h=y;
y=aux;
}

void trocaP(int *a,int *b){
//&a => me dá o endereco 
//*a => me dá o conteúdo apontado pelo endereco
int aux = *a;
*a=*b;
*b=aux;
}

