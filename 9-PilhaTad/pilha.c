/*struct nodo{
	int chave;
	struct nodo *proximo;	
}

struct descritor_pilha{
	int tamanho;
	struct nodo * topo;
}*/

#include "pilha.h"
#include <stdio.h>
#include <stdlib.h>

void push(struct nodo* novoElemento, struct descritor_pilha * minhaPilha){
	
	
}

struct nodo* pop(struct descritor_pilha * minhaPilha){
	
	
}

struct descritor_pilha * cria_descritor_pilha(void){
	struct descritor_pilha *novaPilha = (struct descritor_pilha *)malloc(sizeof(struct descritor_pilha));
	novaPilha->tamanho = 0;
	novaPilha->topo = NULL;
	return novaPilha;
}

struct nodo* cria_nodo(int chave){
	struct nodo *novoNodo = (struct nodo *) malloc(sizeof(struct nodo));
	novoNodo->chave = chave;
	novoNodo->proximo= NULL;
	return novoNodo;
}

void printNodo(struct nodo* elemento){
	printf("nodo com chave %d\n",elemento->chave);
}

void printPilha(struct descritor_pilha* minhaPilha){
	int i;
	printf("tamanho da pilha %d\n",minhaPilha->tamanho);
	struct nodo *temp = minhaPilha->topo;
	while(temp!=NULL){
		printNodo(temp);
		temp = temp->proximo;
	}
}
