#include "pilha.h"
#include <stdio.h>
#include <stdlib.h>

int main(void){
	
	struct descritor_pilha *minhaNovaPilha = cria_descritor_pilha();
	struct nodo *novoElemento = cria_nodo(5);
	push(novoElemento,minhaNovaPilha);
	struct nodo *novoElemento1 = cria_nodo(2);
	push(novoElemento1,minhaNovaPilha);
	struct nodo *novoElemento2 = cria_nodo(1);
	push(novoElemento2,minhaNovaPilha);
	struct nodo *novoElemento3 = cria_nodo(6);
	push(novoElemento3,minhaNovaPilha);
	struct nodo *novoElemento4 = cria_nodo(7);
	push(novoElemento4,minhaNovaPilha);
	printPilha(minhaNovaPilha);
	
	
	
	return 0;
}
