struct nodo{
	int chave;
	struct nodo *proximo;	
};

struct descritor_pilha{
	int tamanho;
	struct nodo * topo;
};


void push(struct nodo* novoElemento, struct descritor_pilha *minhaPilha);
/**função que deleta elementos da pilha**/
struct nodo* pop(struct descritor_pilha * minhaPilha);
struct descritor_pilha * cria_descritor_pilha(void);
struct nodo * cria_nodo(int chave);
void printNodo(struct nodo* elemento);
void printPilha(struct descritor_pilha* minhaPilha);
